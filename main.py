from Tkinter import *
import praw
import subprocess
import threading
import time

class Chat(object):
    def __init__(self):
        self.root = Tk()
        self.root.wm_title('TKKIT - Logged out')

        self.user = praw.Reddit(user_agent="chat")

        self.messagebox = Text(self.root, wrap=WORD)
        self.messagebox.configure(width=60, height=20)
        self.messagebox.pack()

        self.messagebox.tag_configure('old', foreground='gray')
        self.messagebox.tag_configure('you', foreground='blue')

        self.chatholder = Frame(self.root)
        self.chatholder.pack(side=LEFT)

        self.message = Text(self.root)
        self.message.configure(height=3, width=35, wrap=WORD)
        self.message.pack(side=RIGHT)

        self.pmuser = Entry(self.chatholder)
        self.pmuser.configure(width=20)
        self.pmuser.pack(side=TOP)

        self.chatlists = StringVar()
        self.chatlists.set('New Message')

        self.chats = OptionMenu(self.chatholder, self.chatlists, 'New Message')
        self.chats.pack(side=BOTTOM)

        self.message.bind('<Control-backslash>', self.sendmsg)

        self.menu = Menu(self.root)
        self.menu.add_command(label="Login", command=self.loginbox)

        self.root.configure(menu=self.menu)


        self.root.mainloop()


    def loginbox(self):
        self.loginwindow = Toplevel(self.root)
        self.loginwindow.wm_title('Login')
        self.userentry = Entry(self.loginwindow)
        self.userentry.grid(row=0, column=1)
        self.userlabel = Label(self.loginwindow, text="Username:")
        self.userlabel.grid(row=0, column=0)

        self.passwordentry = Entry(self.loginwindow, show="*")
        self.passwordentry.grid(row=1, column=1)
        self.passwordlabel = Label(self.loginwindow, text="Password:")
        self.passwordlabel.grid(row=1, column=0)

        self.loginbutton = Button(self.loginwindow, text="Login", command=lambda: self.login(self.userentry.get(), self.passwordentry.get()))
        self.loginbutton.grid(row=2, columnspan=2)

    def login(self, user, password):
        try:
            self.user.login(user, password)
            self.loginwindow.destroy()
            self.username = user
            self.root.wm_title('TKKIT - '+user)
            self.initialize()
        except praw.errors.InvalidUserPass:
            pass

    def initialize(self):
        m = self.user.get_unread()
        for x in m:
            self.messagebox.insert(END, '\n<'+str(x.author)+'> '+str(x.body), 'old')
            self.messagebox.insert(END, '\n------------------------------------')
            self.messagebox.see(END)
            x.mark_as_read()
        t = threading.Thread(target=self.getloop)
        t.start()

    def getloop(self):
        while 1:
            m = self.user.get_unread()
            for x in m:
                self.messagebox.insert(END, '\n<' + str(x.author) + '> ' + str(x.body))
                self.messagebox.see(END)
                x.mark_as_read()
                subprocess.Popen(['notify-send', 'New message from '+str(x.author)])

            time.sleep(60)

    def sendmsg(self, event=''):
        message = self.message.get('1.0', END)
        recipient = self.pmuser.get()
        self.user.send_message(recipient, 'Message', message)
        self.message.delete('1.0', END)
        self.messagebox.insert(END, '\n<' + str(self.username) + '> ' + str(message), 'you')
        self.messagebox.see(END)


if __name__ == "__main__":
    Chat()